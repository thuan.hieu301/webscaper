import os
from shutil import which 
from pathlib import Path

from scrapy_selenium import SeleniumRequest
import scrapy

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
SELENIUM_DRIVER_NAME = 'firefox'
SELENIUM_DRIVER_ARGUMENTS=['-headless']  # '--headless' if using chrome instead of firefox
gecko_dir = str(Path(ROOT_DIR).parents[2]) + '/driver/geckodriver'
SELENIUM_DRIVER_EXECUTABLE_PATH = which(gecko_dir)


class QuotesSpider(scrapy.Spider):
    name = "quotes"

    def start_requests(self):
        urls = [
            'https://trade.vndirect.com.vn/'
        ]
        for url in urls:
            yield SeleniumRequest(url=url, callback=self.parse)

    def parse(self, response):
        # page = response.url.split("/")[-2]
        page = 'lightning-price'
        filename = 'vndirect-%s.html' % page
        with open(filename, 'wb') as f:
            f.write(response.body)
        self.log('Saved file %s' % filename)